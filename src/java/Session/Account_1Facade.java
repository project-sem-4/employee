/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.Account_1;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Admin
 */
@Stateless
public class Account_1Facade extends AbstractFacade<Account_1> implements Account_1FacadeLocal {

    @PersistenceContext(unitName = "MyEmployeePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public Account_1Facade() {
        super(Account_1.class);
    }

    @Override
    public boolean checkLogin(String username, String password) {
        Query query = em.createNamedQuery("Account.CheckLogin");
        query.setParameter("username", username);
        query.setParameter("password", password);
        List<Account_1> account = query.getResultList();
        
        return !account.isEmpty();
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

   
   
    
}
