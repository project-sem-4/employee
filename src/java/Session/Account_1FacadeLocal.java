/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.Account_1;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Admin
 */
@Local
public interface Account_1FacadeLocal {

    void create(Account_1 account_1);

    void edit(Account_1 account_1);

    void remove(Account_1 account_1);

    Account_1 find(Object id);

    List<Account_1> findAll();

    List<Account_1> findRange(int[] range);

    int count();
    
    boolean checkLogin(String username, String password);
    
    
}
