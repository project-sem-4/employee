/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Admin
 */
@Entity
@Table(name = "ACCOUNT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Account_1.findAll", query = "SELECT a FROM Account_1 a")
    , @NamedQuery(name = "Account_1.findByUsername", query = "SELECT a FROM Account_1 a WHERE a.username = :username")
    , @NamedQuery(name = "Account_1.findByAccountid", query = "SELECT a FROM Account_1 a WHERE a.accountid = :accountid")
    , @NamedQuery(name = "Account_1.findByPassword", query = "SELECT a FROM Account_1 a WHERE a.password = :password")
    , @NamedQuery(name = "Account_1.checkLogin", query = "SELECT a FROM Account_1 a WHERE a.username = :username AND a.password = :password")})
public class Account_1 implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 20)
    @Column(name = "USERNAME")
    private String username;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACCOUNTID")
    private Integer accountid;
    @Size(max = 20)
    @Column(name = "PASSWORD")
    private String password;

    public Account_1() {
    }

    public Account_1(Integer accountid) {
        this.accountid = accountid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getAccountid() {
        return accountid;
    }

    public void setAccountid(Integer accountid) {
        this.accountid = accountid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountid != null ? accountid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Account_1)) {
            return false;
        }
        Account_1 other = (Account_1) object;
        if ((this.accountid == null && other.accountid != null) || (this.accountid != null && !this.accountid.equals(other.accountid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Account_1[ accountid=" + accountid + " ]";
    }
    
}
